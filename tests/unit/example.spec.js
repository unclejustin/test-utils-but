import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";
import Icon from "@/components/Icon.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    const button = wrapper.find({ ref: "testButton" });
    expect(button.find(Icon).exists()).toBe(true);
    expect(wrapper.text()).toMatch(msg);
  });
});
